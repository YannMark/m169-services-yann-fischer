# M169-Services Yann Fischer

### Einleitung allgemein
In diesem Modul tauchten wir in das Thema Microservices mit Schwerpunkt Vagrant und Docker ein.

Bei Vagrant erhielten wir einen einfachen Einblick wie das ganze prinzipziell funktioniert.

Anschliessend arbeiteten wir an einem selbst definierten Projekt mit Docker.
### Inhaltsverzeichnis

* [10 Infrastruktur Automatisierung](10-Infrastruktur/)
: Aubau unserer Umgebung + Cheatsheet für Markdown Files
* [20 Container](20-Container/)
: Installation und Befehle für Docker
* [30 Images](30-Images/)
: Beinhaltet Bilder für Unterricht (LB2 verfügt über seperaten Image Folder)
* [LB2 Nginx Webserver](LB2/)
: Dokumentation der praktischen Arbeit