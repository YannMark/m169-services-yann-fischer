# Konfiguration von Webserver
VM in VM aufgesetzt mittels vagrant

## Aufbau Umgebung

Die gute TBZ stellen mehrere VMs zur Verfügung.

Mithilfe von Wireguard (VPN-Tunnel) und SSH greifen wir auf unsere VMs zu.

Login: Ubuntu PW: bei_mir_Sup3rgehe1m!

## Vagrant Stuff

vagrant init ubuntu/xenial64 zum erstellen von Vagrantfile

### Inhalt Vagrantfile

Vagrant.configure ("2") do |config|
config.vm.box = "ubuntu/xenial64"

config.cm.hostname = "web-dev"

config.vm.provision "shell", path: "provision.sh"

config.vm.network "forwarded_port", guest: 80, host:8090, id: "nginx-cal"

config.vm.synced_folder ".", "/vagrant", disabled: true

config.vm.syced_folder "www", "/vagrant/www"

end

### Weiter Befehle für Vagrant Steuerung
![](../images/Vagrant-CheatSheet.png)
