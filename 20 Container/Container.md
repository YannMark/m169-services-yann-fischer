# Webservice in Container zur Verfügung stellen

## Repository mit Dockerfile herunterladen und auf VM bugsieren
1. Repository von Calisto lokal downloaden<br>
`git clone git@gitlab.com:ser-cal/docker-bootstrap.git`
2. Repo auf VM kopieren<br>
`scp docker-bootstrap-main ubuntu@10.1.44.17:/home/ubuntu`
3. In VM einloggen und in Directory vom Dockerfile wechseln<br>
`cd docker-bootstrap/first-container`
4. Ins Repo einloggen<br>
`docker login registry.gitlab.com`<br>
Benutzername und Passwort angeben


![](../images/docker-cheat-sheet.jpg)