
[TOC]

# 1. Einleitung
Im Modul M169 wurde das Ziel verfolgt, einen Webserver in einem Container zu starten und dabei sicherzustellen, dass die zur Ausführung benötigten HTML-Dateien auf dem Hostsystem der Containerumgebung persistiert bleiben. Dies erforderte ein Verständnis dafür, wie Container arbeiten und wie sie eingesetzt werden können, um eine Anwendung auf eine effiziente und sichere Weise bereitzustellen.

Als Host für die Containerumgebung wurde uns eine virtuelle Ubuntu-Maschine zur Verfügung gestellt, die in der TBZ Cloud gehostet wird. Die Verwendung einer virtuellen Maschine ermöglichte es uns, eine isolierte Umgebung zu schaffen, in der der Webserver und die benötigten Ressourcen laufen konnten, ohne dass andere Anwendungen oder Prozesse beeinträchtigt wurden.

Durch die Implementierung einer Container-basierten Architektur konnten wir die Skalierbarkeit und Portabilität unserer Anwendung erhöhen und gleichzeitig sicherstellen, dass sie ressourceneffizient betrieben wird. Dies ist besonders wichtig, da viele moderne Anwendungen in Cloud-Umgebungen ausgeführt werden, in denen Ressourcen knapp und teuer sind.

Insgesamt war das Abschlussprojekt des Moduls M169 eine wertvolle Erfahrung, die uns nicht nur half, unsere Fähigkeiten im Bereich der Containerisierung und der Anwendungsbereitstellung zu verbessern, sondern auch ein Verständnis dafür zu entwickeln, wie moderne Anwendungen in der Cloud betrieben werden können.

# 2. Service Beschreibung
In einem Container wird eine Instanz eines Nginx-Webserver ausgeführt.

Nginx ist einen Open-Source-Webserver-Software. Bekannt ist diese für ihre hohe Leistung, Skalierbarkeit und Flexibilität.

In diesem Projekt wird nur eine einzelne Instanz des Apps laufen gelassen. Es ist jedoch möglich mithilfe von Orchestrierungstools, wie zum Beispiel Kubernetes, mehrere Instanzen automatisch zu skalieren und verwalten. Damit wird eine hohe Verfügbarkeit und gute Lastverteilung gewährleistet.

# 3. Umsetzung
## 3.1 Vorbereitung von Hostmaschine
### Zugrif auf die Host-Maschine
Für den Zugriff auf die virtuelle Maschine muss zuerst ein VPN Tunnel mit zu der Hostmaschine aufgebuat werden.

In diesem Fall haben wir uns für Wireguard entschieden:

![](./images/Wireguard.png)

Die LoginDaten für seine VM findet kann man finden indem man nach der IP der Maschine in einem Webbrowser:

![](./images/Password.png)

Hat man das Password, und die VPN Verbindung steht, so kann man sich mithilfe von ssh von der command Zeile aus anmelden:

```
$ ssh 10.1.44.17 -l ubuntu
```

Die Option -l definiert den User, welchen mit welchem man auf die Maschine einloggen will. In unserem Falle ist dies der User ubuntu. Anschliessend muss nach noch das Password eingeben. Funktioniert alles, so sollte es in etwa so aussehen:

![](./images/Login.png)

## 3.2 Updaten und Installation von Docker
Mit den folgenden Befehlen wird das Linux-System up to-date gebracht:
```
$ sudo apt-get update
$ sudo apt-get upgrade
```
Docker ist auf unserer VM schon vorinstalliert. Wenn nicht, so kann man es mithilfe von sudo apt-install docker herunterladen und installieren.
```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
Man kann die Installation mithilfe von folgendem Befehl testen:
```
$ sudo docker run hello-world
```
![](./images/Helloworld.png)

Docker funktioniert nun.

Nun ist die VM soweit bereit für den Einsatz.

## 3.3 Einrichten von lokalem Verzeichniss
Der Nginx-Server speichert seine angezeigte Seiten in HTML-Files.

Wir erstellen ein neues Verzeichniss im Homeverzeichniss unseres Users:
```
 $ sudo mkdir nginx-html
```
Darin erstellen wir mittels nano zwei neue html-Files index.html und about.html:
```
$ sudo nano index.html
```
Das Verzeichniss sieht am Ende etwa so aus:

![](./images/Verzeichniss.png)

Die HTML-Files beinhalten dabei den Code für die Webseite. In diesem Fall ein kurzer Text mit einem Button der auf die jeweils andere Seite weiterleitet:
![](./images/html.png)

## 3.4 Testlauf mit Original Nginx-Image
Optional: Wenn man will, kann man Nginx-Image in die lokale Registry herunterladen:

![](./images/nginxpull.png)

Anschliessend kann man mit folgendem Befehl prüfen, ob das Image korrekt herunterkopiert wurde:
```
 $ docker images
```


## 3.5 Nginx-Image neu taggen und auf Gitlab Repository hochladen
Bevor man beginnt, muss man sich in seiner Registry einloggen:
```
$ sudo docker login registry.gitlab.com
```
![](./images/Gitlablogin.png)

Da ich mich schon früher eingeloggt habe, sind meine Logincredentials schon geschpeichert.

Nun lädt man das benötigte Image herunter und verteilt einen neuen tag:

(Mit dem befehl docker images kann man prüfen, ob ein neues image erstellt wurde)

![](./images/newtag.png)

Zum Schluss pusht man das Image mit folgendem Befehl in sein Repository hochladen:

![](./images/inrepopushen.png)

Das Image ist nun im Git-Repository sichtbar:

![](./images/Gitpush.png)

Um nun noch sicher zu stellen, dass man das Image aus seinem eigenen Repository verwendet löscht man alle vorhandenen Images mit
```
$ docker rmi "image-id"
```
![](./images/CleanImages.png)

Anschliessend zieht man sein eigenes mit folgendem Befehl wieder aus dem Repository runter:
```
$ docker pull registry.gitlab.com/yannmark/m169-services-yann-fischer:lb2
```

## 3.6 Container mit persistenten Daten starten
Mit folgendem Befehl wird eine Instanz des Webserver mit den persistenten Daten des Hostsystems gestartet:
```
$ docker run -d -p 8090:80 -v /home/ubuntu/nginx-html:/usr/share/nginx/html --name nginx-web registry.gitlab.com/yannmark/m169-services-yann-fischer:lb2
```
![](./images/Container%20starten.png)

### Erläuterung der einzelnen Optionen:
- -d lässt den Container "detached" laufen. Dies bedeutet, dass der Container läuft, ohne dass man sich aktiv in diesem aufhalten muss.
- -p gibt den Port an, über welchen man auf den Container zugreifen möchte. In diesem Fall kann man von aussen über den Port 8090 auf den Webserver zugreifen.
- -v verknüpft ein vorhandenes Volume mit dem Container. Dies ist in unserem Fall wichtig, da uns diese Option erlaubt unsere persistenten Daten mit dem Container zu benutzen. Syntax: "Pfad pers. Daten":"Zielpfad des Containers"
- --name gibt den Namen des Containers an

# 4. Testing
## 4.1. Webserver Zugriff
Läuft der Container so kann man über einen Webbrowser auf den Container zugreifen.

Man muss dabei beachten, dass man zusätzlich zur IP des Servers auch noch den beim Erstellen definierten Port angibt. In meniem Beispiel ist sieht die Adresse wie folgt aus:
10.1.44.17:8090

Nun sieht man die vorher im http-File codierte Seite:

![](./images/webpage.png)

Wenn man will, kann man den Container mit diesem Befehl stoppen.

```
$ docker stop "container ID"
```
Reloaded man nun die Webseite, so kann man nicht mehr darauf zugreifen.

## 4.2. Persistente Daten Testen
### 4.2.1 Container ohne persitente Daten
Man kann zu testzwecken einen weiteren Container erstellen. Dabei lässt man jedoch die Option -v weg.

Greift man auf den neu erstellten Container zu, so wird man die standard nginx-Webpage vorfinden:

![](./images/Nginx%20standard.png)

### 4.2.2. Anpassungen am HTML File vornehmen
Passt man das lokal gespeicherte HTML File des Webservers an, 

![](./images/Anpassungen.png)

so werden die Änderungen nach einem Reload der Webpage aktualisiert angezeigt.

![](./images/Proof.png)

# 5. Quellen
- Tutorial Nginx + persistent Files: https://www.youtube.com/watch?v=mgwo8fq-SkA&ab_channel=MafiaCodes
- Doku Docker run: https://docs.docker.com/engine/reference/run/
- Chatgpt vor allem für html-file: https://chat.openai.com/